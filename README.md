## Question

Can we install a Node package directly from GitHub?

## Answer

Yes, we can!

Syntax:

```
$ npm i github:<githubname>/<githubrepo>[#semver:<semver>]
```

Example:

```
$ npm i github:moment/moment#semver:2.29.1
```
