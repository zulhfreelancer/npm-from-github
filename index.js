const moment = require("moment");

const date = moment();
const formatted = date.format("DD-MM-YYYY");
console.log(formatted); // Will show 03-11-2020 for 3rd November 2020
